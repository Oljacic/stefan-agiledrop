<?php
/**
 * @file
 * Contains \Drupal\ad_general\Controller\FirstController.
 */

namespace Drupal\ad_general\Controller;

use Drupal\Core\Controller\ControllerBase;

class FirstController extends ControllerBase {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('Hello world'),
    );
  }
}