<?php

/* {# inline_template_start #}<div class="event-organizer">
<div class="logo-titles">
{{ field_logo }}
<p>{{ title }}</p>
</div>
<div class="field-body">{{ body }}</div>
<div class="field-link">{{ view_node }}</div>
</div>


<div class="event-location">
<div class="logo-titles">
{{ field_location_image }}
<p>{{ title_1 }}</p>
</div>
<div class="field-body">{{ body_1 }}</div>
<div class="field-link">{{ view_node_1 }}</div>
</div> */
class __TwigTemplate_ab908a2a8075daba4d52f6b1c8fcfe549e1e193f80869755b54724b3f11551f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"event-organizer\">
<div class=\"logo-titles\">
";
        // line 3
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_logo"]) ? $context["field_logo"] : null), "html", null, true));
        echo "
<p>";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo "</p>
</div>
<div class=\"field-body\">";
        // line 6
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["body"]) ? $context["body"] : null), "html", null, true));
        echo "</div>
<div class=\"field-link\">";
        // line 7
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["view_node"]) ? $context["view_node"] : null), "html", null, true));
        echo "</div>
</div>


<div class=\"event-location\">
<div class=\"logo-titles\">
";
        // line 13
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["field_location_image"]) ? $context["field_location_image"] : null), "html", null, true));
        echo "
<p>";
        // line 14
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_1"]) ? $context["title_1"] : null), "html", null, true));
        echo "</p>
</div>
<div class=\"field-body\">";
        // line 16
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["body_1"]) ? $context["body_1"] : null), "html", null, true));
        echo "</div>
<div class=\"field-link\">";
        // line 17
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["view_node_1"]) ? $context["view_node_1"] : null), "html", null, true));
        echo "</div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"event-organizer\">
<div class=\"logo-titles\">
{{ field_logo }}
<p>{{ title }}</p>
</div>
<div class=\"field-body\">{{ body }}</div>
<div class=\"field-link\">{{ view_node }}</div>
</div>


<div class=\"event-location\">
<div class=\"logo-titles\">
{{ field_location_image }}
<p>{{ title_1 }}</p>
</div>
<div class=\"field-body\">{{ body_1 }}</div>
<div class=\"field-link\">{{ view_node_1 }}</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 17,  95 => 16,  90 => 14,  86 => 13,  77 => 7,  73 => 6,  68 => 4,  64 => 3,  60 => 1,);
    }

    public function getSource()
    {
        return "{# inline_template_start #}<div class=\"event-organizer\">
<div class=\"logo-titles\">
{{ field_logo }}
<p>{{ title }}</p>
</div>
<div class=\"field-body\">{{ body }}</div>
<div class=\"field-link\">{{ view_node }}</div>
</div>


<div class=\"event-location\">
<div class=\"logo-titles\">
{{ field_location_image }}
<p>{{ title_1 }}</p>
</div>
<div class=\"field-body\">{{ body_1 }}</div>
<div class=\"field-link\">{{ view_node_1 }}</div>
</div>";
    }
}
