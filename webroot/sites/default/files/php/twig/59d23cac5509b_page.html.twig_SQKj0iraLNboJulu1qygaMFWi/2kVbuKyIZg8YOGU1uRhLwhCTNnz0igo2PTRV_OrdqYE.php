<?php

/* themes/custom/agiledrop_kick/templates/page.html.twig */
class __TwigTemplate_f85c2b2ef9f191970655dcc39b74238680f25adf1c83cc7584f9080702849ec8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 22);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            ";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "
        </div>
    </div>
    <div class=\"primary-menu\">
        ";
        // line 8
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "primary_menu", array()), "html", null, true));
        echo "
    </div>

    <header role=\"banner\">
        ";
        // line 12
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 18
        echo "        ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_search", array()), "html", null, true));
        echo "

        <div class=\"layout-content-first\">
            ";
        // line 21
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
            ";
        // line 22
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) {
            // line 23
            echo "            <aside class=\"layout-sidebar-first\" role=\"complementary\">
            ";
            // line 24
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()), "html", null, true));
            echo "
            </aside>
            ";
        }
        // line 27
        echo "
            ";
        // line 28
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())) {
            // line 29
            echo "            <aside class=\"layout-sidebar-second\" role=\"complementary\">
            ";
            // line 30
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()), "html", null, true));
            echo "
            </aside>
            ";
        }
        // line 33
        echo "        </div>";
        // line 34
        echo "
        ";
        // line 35
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array())) {
            // line 36
            echo "            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    ";
            // line 38
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array()), "html", null, true));
            echo "
                </div>
            </div>
        ";
        }
        // line 42
        echo "    </main>

    ";
        // line 45
        echo "        ";
        // line 46
        echo "            ";
        // line 47
        echo "        ";
        // line 48
        echo "    ";
        // line 49
        echo "
    ";
        // line 51
        echo "        ";
        // line 52
        echo "            ";
        // line 53
        echo "        ";
        // line 54
        echo "    ";
        // line 55
        echo "
    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            ";
        // line 58
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array()), "html", null, true));
        echo "
        </footer>
    </div>

    <div class=\"footer\">
        <footer role=\"contentinfo\">
            ";
        // line 64
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_mid", array()), "html", null, true));
        echo "
        </footer>
    </div>

    <div class=\"footer-last\">
        <footer role=\"contentinfo\">
            ";
        // line 70
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo "
        </footer>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/agiledrop_kick/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  170 => 70,  161 => 64,  152 => 58,  147 => 55,  145 => 54,  143 => 53,  141 => 52,  139 => 51,  136 => 49,  134 => 48,  132 => 47,  130 => 46,  128 => 45,  124 => 42,  117 => 38,  113 => 36,  111 => 35,  108 => 34,  106 => 33,  100 => 30,  97 => 29,  95 => 28,  92 => 27,  86 => 24,  83 => 23,  81 => 22,  77 => 21,  70 => 18,  62 => 12,  55 => 8,  48 => 4,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            {{ page.highlighted }}
        </div>
    </div>
    <div class=\"primary-menu\">
        {{ page.primary_menu }}
    </div>

    <header role=\"banner\">
        {{ page.header }}
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>{# link is in html.html.twig #}
        {{ page.content_search }}

        <div class=\"layout-content-first\">
            {{ page.content }}
            {% if page.sidebar_first %}
            <aside class=\"layout-sidebar-first\" role=\"complementary\">
            {{ page.sidebar_first }}
            </aside>
            {% endif %}

            {% if page.sidebar_second %}
            <aside class=\"layout-sidebar-second\" role=\"complementary\">
            {{ page.sidebar_second }}
            </aside>
            {% endif %}
        </div>{# /.layout-content #}

        {% if page.content_second %}
            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    {{ page.content_second }}
                </div>
            </div>
        {% endif %}
    </main>

    {#{% if page.sidebar_first %}#}
        {#<aside class=\"layout-sidebar-first\" role=\"complementary\">#}
            {#{{ page.sidebar_first }}#}
        {#</aside>#}
    {#{% endif %}#}

    {#{% if page.sidebar_second %}#}
        {#<aside class=\"layout-sidebar-second\" role=\"complementary\">#}
            {#{{ page.sidebar_second }}#}
        {#</aside>#}
    {#{% endif %}#}

    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            {{ page.footer_top }}
        </footer>
    </div>

    <div class=\"footer\">
        <footer role=\"contentinfo\">
            {{ page.footer_mid }}
        </footer>
    </div>

    <div class=\"footer-last\">
        <footer role=\"contentinfo\">
            {{ page.footer }}
        </footer>
    </div>

</div>{# /.layout-container #}";
    }
}
