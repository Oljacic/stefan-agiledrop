<?php

/* themes/custom/agiledrop_kick/templates/page--companies.html.twig */
class __TwigTemplate_7aba1e46a339a683779a57e036f5c2394fedc2ebeee4aa404852bc7bc1e2124a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 31);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            ";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "
        </div>
    </div>
    <div class=\"primary-menu\">
        ";
        // line 8
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "primary_menu", array()), "html", null, true));
        echo "
    </div>

    <header role=\"banner\">
        ";
        // line 12
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 18
        echo "
        <div class=\"fluid\">
            <div class=\"search-filter\">
                ";
        // line 21
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_search", array()), "html", null, true));
        echo "
            </div>
        </div>


        <div class=\"layout-content-first\">
            ";
        // line 27
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
        </div>";
        // line 29
        echo "

        ";
        // line 31
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array())) {
            // line 32
            echo "            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    ";
            // line 34
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array()), "html", null, true));
            echo "
                </div>
            </div>
        ";
        }
        // line 38
        echo "    </main>

    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            ";
        // line 42
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array()), "html", null, true));
        echo "
        </footer>
    </div>

    <div class=\"footer\">
        <footer role=\"contentinfo\">
            ";
        // line 48
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_mid", array()), "html", null, true));
        echo "
        </footer>
    </div>

    <div class=\"footer-last\">
        <footer role=\"contentinfo\">
            ";
        // line 54
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo "
        </footer>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/agiledrop_kick/templates/page--companies.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 54,  120 => 48,  111 => 42,  105 => 38,  98 => 34,  94 => 32,  92 => 31,  88 => 29,  84 => 27,  75 => 21,  70 => 18,  62 => 12,  55 => 8,  48 => 4,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            {{ page.highlighted }}
        </div>
    </div>
    <div class=\"primary-menu\">
        {{ page.primary_menu }}
    </div>

    <header role=\"banner\">
        {{ page.header }}
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>{# link is in html.html.twig #}

        <div class=\"fluid\">
            <div class=\"search-filter\">
                {{ page.content_search }}
            </div>
        </div>


        <div class=\"layout-content-first\">
            {{ page.content }}
        </div>{# /.layout-content #}


        {% if page.content_second %}
            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    {{ page.content_second }}
                </div>
            </div>
        {% endif %}
    </main>

    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            {{ page.footer_top }}
        </footer>
    </div>

    <div class=\"footer\">
        <footer role=\"contentinfo\">
            {{ page.footer_mid }}
        </footer>
    </div>

    <div class=\"footer-last\">
        <footer role=\"contentinfo\">
            {{ page.footer }}
        </footer>
    </div>

</div>{# /.layout-container #}";
    }
}
