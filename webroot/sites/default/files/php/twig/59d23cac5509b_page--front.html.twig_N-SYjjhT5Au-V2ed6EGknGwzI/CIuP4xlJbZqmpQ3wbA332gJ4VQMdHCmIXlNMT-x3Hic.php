<?php

/* themes/custom/agiledrop_kick/templates/page--front.html.twig */
class __TwigTemplate_0eec4a8e7571e0e90e69c87e7e3026c04710bc7929e672ba3535aeff8bf2e76e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 27);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            ";
        // line 4
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "
        </div>
    </div>
    <div class=\"primary-menu\">
        ";
        // line 8
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "primary_menu", array()), "html", null, true));
        echo "
    </div>

    <header role=\"banner\">
        ";
        // line 12
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>";
        // line 18
        echo "        ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_search", array()), "html", null, true));
        echo "

        <div class=\"layout-content-first\">
            <div class =\"content-flex\">
                ";
        // line 22
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
            </div>
        </div>";
        // line 25
        echo "

        ";
        // line 27
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array())) {
            // line 28
            echo "            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    ";
            // line 30
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content_second", array()), "html", null, true));
            echo "
                </div>
            </div>
        ";
        }
        // line 34
        echo "    </main>

    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            ";
        // line 38
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array()), "html", null, true));
        echo "
        </footer>
    </div>

     <div class=\"footer\">
         <footer role=\"contentinfo\">
             ";
        // line 44
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_mid", array()), "html", null, true));
        echo "
         </footer>
     </div>

     <div class=\"footer-last\">
         <footer role=\"contentinfo\">
             ";
        // line 50
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo "
         </footer>
     </div>

</div>";
    }

    public function getTemplateName()
    {
        return "themes/custom/agiledrop_kick/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 50,  115 => 44,  106 => 38,  100 => 34,  93 => 30,  89 => 28,  87 => 27,  83 => 25,  78 => 22,  70 => 18,  62 => 12,  55 => 8,  48 => 4,  43 => 1,);
    }

    public function getSource()
    {
        return "<div class=\"layout-container\">
    <div class=\"top-header\">
        <div class=\"top-container\">
            {{ page.highlighted }}
        </div>
    </div>
    <div class=\"primary-menu\">
        {{ page.primary_menu }}
    </div>

    <header role=\"banner\">
        {{ page.header }}
    </header>


    <main role=\"main\">
        <a id=\"main-content\" tabindex=\"-1\"></a>{# link is in html.html.twig #}
        {{ page.content_search }}

        <div class=\"layout-content-first\">
            <div class =\"content-flex\">
                {{ page.content }}
            </div>
        </div>{# /.layout-content #}


        {% if page.content_second %}
            <div class=\"layout-second\">
                <div class=\"layout-width\">
                    {{ page.content_second }}
                </div>
            </div>
        {% endif %}
    </main>

    <div class=\"footer-top\">
        <footer role=\"contentinfo\">
            {{ page.footer_top }}
        </footer>
    </div>

     <div class=\"footer\">
         <footer role=\"contentinfo\">
             {{ page.footer_mid }}
         </footer>
     </div>

     <div class=\"footer-last\">
         <footer role=\"contentinfo\">
             {{ page.footer }}
         </footer>
     </div>

</div>{# /.layout-container #}";
    }
}
